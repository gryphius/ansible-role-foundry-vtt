# Foundry Virtual Tabletop Server

This role will set up Foundry VTT on a linux server and create a systemd service to automatically start the server.

 * Prerequisites: 
   * Nodejs 12.0+ is assumed to be present
   * The installation file for the nodejs server (foundryvtt-x.y.z.zip) must be copied to a place where the server can access it. Either place it on the server itself or put it on a webserver. This ansible role can not currently download the original file automatically as this requires a logged in user


This role will:

 * create a user/group to run foundryvtt
 * create a folder to where the contents of the zip file will be extracted into ( `/usr/local/foundryvtt/<version>` by default )
 * create a data folder ( `/usr/local/foundryvtt/data` by default )
 * create a symlink which points to the current version. This allows to upgrade to a new version without destroying the previous one. ( `/usr/local/foundryvtt/current`  by default )
 * create systemd service file (so you can do `systemctl start/stop foundryvtt` )
 * manage the foundry vtt `options.json` configuration file.

The following variables must be set when using this role:

 * `foundryvtt_version` 
 * `foundryvtt_download_url`


Additional options can be set, for the full list, see [defaults/main.yml](defaults/main.yml)

Example playbook(simple):

```yaml 
- hosts: foundryvtt
  roles:
    - role: foundryvtt
      foundryvtt_version: "0.5.5"
      foundryvtt_download_url: "https://path/to/your/copy/of/foundryvtt.0.5.5.zip" # can also be a local file on the server
  ```

Example with additional options:

```yaml 
- hosts: foundryvtt
  roles:
    - role: foundryvtt
      foundryvtt_version: "0.5.5"
      foundryvtt_download_url: "https://path/to/your/copy/of/foundryvtt.0.5.5.zip" # can also be a local file on the server
      foundryvtt_extra_args: "--noupnp"
      foundryvtt_adminkey: "3eac0f0f0f1d38d6de669d248814423e2ef021e8e36c9a61e182ea29be1b150d3cffc23640240857c1547744739054bed005d527216dd860de22d3bd7a58d3eb"
      foundryvtt_proxyssl: true
      foundryvtt_proxyport: 443
      foundryvtt_hostname: foundryvtt.example.com
  ```

